<?php

namespace App\Exports;

use App\Entities\Customer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Events\AfterSheet;


class CustomerExport implements ShouldAutoSize, FromQuery, WithHeadings, WithMapping, WithEvents
{
    use Exportable;

    private static $count = '1';

    public function __construct(string $dealer,$start,$end)
    {
        $this->dealer = $dealer;
        $this->start = $start;
        $this->end = $end;
    }

    public function headings(): array
    {
        return [
          '#',
          'KODE',
          'NAMA DEALER',
          'NAMA CUST',
          'NOPOL',
          'NO W/O',
          'VOUCHER',
          'TANGGAL REDEEM',
        ];
    }

    public function registerEvents(): array
    {
        return [
        AfterSheet::class => function (AfterSheet $event) {
            $cell = 'A1:E10000';
            $centerAlign = [
            'alignment' => [
              'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
              'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
          ];
          $event->sheet->getDelegate()->getStyle($cell)->applyFromArray($centerAlign);
          $event->sheet->getDelegate()->getStyle('A1:W1')->getFont()->setSize(14);
      },
    ];
    }

    public function map($customer): array
    {
        return [
          Self::$count++,
          $customer->dealer->dealer_code,
          $customer->dealer->name,
          $customer->name,
          $customer->no_pol,
          $customer->work_order,
          number_format($customer->angpao->bucket->nominal),
          \Carbon\Carbon::parse($customer->angpao->created_at)->format('d-M-Y'),
        ];
    }

    public function query()
    {
        $carbonStart = $this->start;
        $carbonEnd = $this->end;

        if ($this->dealer == "all") {
          $customer = Customer::whereHas('angpao',function($e)use($carbonStart,$carbonEnd){
            $e->whereBetween('created_at',[$carbonStart,$carbonEnd]);
          })->orderBy('name',"ASC");
          return $customer;
        }else{
          $customer = Customer::where('id_user',$this->dealer)->whereHas('angpao',function($e)use($carbonStart,$carbonEnd){
            $e->whereBetween('created_at',[$carbonStart,$carbonEnd]);
          })->orderBy('name',"ASC");
          return $customer;
        }
    }
}
