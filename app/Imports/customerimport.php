<?php

namespace App\Imports;

use App\Entities\Customer;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
// use Maatwebsite\Excel\Imports\HeadingRowFormatter;

// HeadingRowFormatter::default('none');

class customerimport implements ToModel, withStartRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    //column kebawah
    public function model(array $column)
    {
        return new Customer([
            'id_user' => request()->user()->id,
            'no_pol' => $column[0],
            'work_order' => $column[1], 
            ]);
    }
    //row kesamping
    public function startRow(): int {
        return 2;
    }
}
