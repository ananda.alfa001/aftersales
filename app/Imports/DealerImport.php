<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Hash;

class DealerImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
      $user = User::where('dealer_code',$row["dealer_code"])->first();

      if (empty($row['dealer_code'])) {
        return null;
      }

      if ($user == null) {
          $password = $this->generate_unique_int(4);
          $check = User::where('password_real', $password)->count();

          while ($check != 0) {
              $password = generate_unique_int(4);
              $check = User::where('password_real', $password)->count();
          }

          $user = User::create([
            'name'     => $row['dealer_name'],
            'dealer_code'    => $row['dealer_code'],
            'password' => Hash::make($password),
            'password_real'    => $password,
            'city'    => $row['city'],
            'provance'    => $row['province'],
          ]);

          $user->assignRole("Dealer");
        }
    }

    function generate_unique_int($lengthint =null)
    {
        $strstring = "angpao";

        $int = "123456789";
        $strint = substr(str_shuffle(str_repeat($int, 5)), 0, $lengthint);

        return $strstring.$strint;
    }




}
