<?php

namespace App\Http\Controllers\Dealer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Bucket;
use App\Entities\Customer;
use App\Traits\Responder;
use Validator;
use App\Imports\CustomerImport;
use Maatwebsite\Excel\Facades\Excel;

class DealerController extends Controller
{
  use Responder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['buckets'] = Bucket::all();
        return view('dealer.dashboard.index',$data);
    }

    public function AddCustomerIndex()
    {
        return view('dealer.customer.addcustomer');
    }

    public function SearchCustomerIndex(Request $request)
    {

        $data['customer'] = Customer::where('id_user',$request->user()->id)->where(function($ee) use($request){
          $ee->where('name','like',"%".$request->customer."%")->orWhere('no_pol','like',"%".$request->no_polisi."%")->orWhere('work_order','like',"%".$request->no_work."%")
          ->orWhereHas('angpao',function($e) use ($request){
            $e->where('code','like',"%".$request->code_angpao."%");
          });
        })->get();

        return view('dealer.customer.search',$data);
    }

    public function ListCustomerIndex()
    {
        $data['custhasntangpao'] = Customer::doesnthave('angpao')->get();
        $data['custhasangpao'] = Customer::has('angpao')->get();
        return view('dealer.customer.list',$data);
    }

    public function createCustomer(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'no_pol' => 'required',
        'work_order' => 'required',
      ]);

      if ($validator->passes()) {
        $customer = new Customer();
        $checkCustomer = Customer::where('no_pol',$request->no_pol)->orWhere('work_order',$request->work_order)->count();
        if ($checkCustomer != 0) {
          $response['message'] = array_merge(['Data sudah ada']);
          return $this->response(400, $response);
        }
        $customer->no_pol = $request->no_pol;
        $customer->id_user = request()->user()->id;
        $customer->work_order = $request->work_order;

        if ($customer->save()) {
          $response['redirect_url'] = route('dealer.addcustomer');
          return $this->response(200, $response);
        }
      }else{
        $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
        return $this->response(400, $response);
      }
    }
    
    public function import(request $request){
      $rules = [
        'file' => 'required|mimes:csv,xls,xlsx'
      ];
      $validator = \Validator::make($request->all(), $rules);
      if($validator->fails()) {
        $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
        return $this->response(400, $response);
      }
      $file = $request->file('file');
      $nama_file = rand().$file->getClientOriginalName();
      $file->move('file_customer',$nama_file);
      Excel::import(new Customerimport, public_path('/file_customer/'.$nama_file));

      $response['redirect_url'] = route('dealer.addcustomer');
      return $this->response(200, $response);
          // return redirect('/dealer/add-customer');
      }
  

    public function print($id)
    {
        $data['customer'] = Customer::findOrFail($id);
        return view('dealer.customer.print',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $customer = Customer::find($id);
      $data['customer'] = $customer;
      return $this->response(200, $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
          'no_pol' => 'required',
          'work_order' => 'required',
        ]);

        if ($validator->passes()) {
          $customer = Customer::where('id',$id)->doesnthave('angpao')->first();
          if ($customer == null) {
            $response['message'] = array_merge(['data sudah mempunyai angpao dan tidak bisa di hapus']);
            return $this->response(400, $response);
          }
          $customer->name = $request->name;
          $customer->no_pol = $request->no_pol;
          $customer->work_order = $request->work_order;

          if ($customer->save()) {
            if ($request->type == "search") {
              $response['redirect_url'] = route('dealer.searchcustomer');
              return $this->response(200, $response);
            }else{
              $response['redirect_url'] = route('dealer.listcustomer');
              return $this->response(200, $response);
            }

          }
        }else{
          $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
          return $this->response(400, $response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
