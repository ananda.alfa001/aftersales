<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Entities\Configuration;

class AuthAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check() == true ) {
            if (Auth::user()->getRoleNames()->first() == "Dealer"){
              return redirect()->route('dealer.index');
            }else{
              return redirect()->route('superadmin.index');
            }
        } else {
            return view('auth.login-admin');
        }
    }

    public function doLogin(Request $request)
    {
        if ($request->ajax()) {
            $credentials = ['dealer_code' => $request->get('dealer_code'), 'password' => $request->get('password')];
            $rules = [
              'dealer_code' => 'required',
              'password' => 'required|min:8|max:16',
            ];

            $validator = Validator::make($request->all(), $rules);
            $config = Configuration::findOrFail(1);
            if ($validator->fails() || Auth::attempt($credentials) == false) {
                $message = array_merge(['Login Failed!'], $validator->errors()->all());
                $request->session()->put('old_email', $request->email);

                return response(['status' => false, 'message' => $message]);
            } elseif ($validator->passes() && Auth::attempt($credentials) == true) {

                if ($config->maintenance == 1) {
                    if (Auth::user()->getRoleNames()->first() == "Dealer") {
                      Auth::logout();
                      $request->session()->flush();
                      $message = array_merge(['Mohon maaf, campaign tidak dapat diakses pada Hari Minggu & Hari Libur Nasional']);

                      return response(['status' => false, 'message' => $message]);
                    }else{
                      $request->session()->regenerate();
                      $role = Auth::user()->getRoleNames()->first();
                      $message = array_merge(['Login Success!'], ['Redirecting page ...']);

                      return response(['status' => true,'role' => $role , 'message' => $message ]);
                    }
                }else{
                  $request->session()->regenerate();
                  $role = Auth::user()->getRoleNames()->first();
                  $message = array_merge(['Login Success!'], ['Redirecting page ...']);

                  return response(['status' => true,'role' => $role , 'message' => $message ]);
                }

            }
          } else {
              return redirect()->route('login');
          }
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->flush();
        return redirect()->route('admin-mitsubishi');
    }
}
