<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Bucket;
use App\Traits\Responder;
use Validator;

class AngpaoController extends Controller
{
    use Responder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['buckets'] = Bucket::orderBy('nominal','DESC')->get();

        return view('superadmin.angpao.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function addQty(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
          'qty' => 'required',
        ]);

        if ($validator->passes()) {
          $bucket = Bucket::find($id);
          $bucket->qty = $bucket->qty + $request->qty;

          if ($bucket->save()) {
            $response['redirect_url'] = route('superadmin.angpao.index');
            return $this->response(200, $response);
          }
        }else{
          $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
          return $this->response(400, $response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
