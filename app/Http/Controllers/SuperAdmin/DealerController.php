<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Imports\DealerImport;
use App\User;
use App\Traits\Responder;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Hash;

class DealerController extends Controller
{
    use Responder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['dealers'] = User::role('Dealer')->get();
      return view('superadmin.dealer.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'name' => 'required',
          'dealer_code' => 'required',
        ]);

        if ($validator->passes()) {
          $dealer =  new User();
          $dealer->name = $request->name;
          $dealer->dealer_code = $request->dealer_code;
          $password = $this->generate_unique_int(4);
          $check = User::where('password_real', $password)->count();

          while ($check != 0) {
              $password = generate_unique_int(4);
              $check = User::where('password_real', $password)->count();
          }

          $dealer->password = Hash::make($password);
          $dealer->password_real = $password;
          $dealer->city = $request->city;
          $dealer->provance = $request->provance;

          if ($dealer->save()) {
            $dealer->assignRole('Dealer');
            $response['redirect_url'] = route('superadmin.dealer.index');
            return $this->response(200, $response);
          }
        }else{
          $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
          return $this->response(400, $response);
        }
    }

    function generate_unique_int($lengthint =null)
    {
        $strstring = "angpao";

        $int = "123456789";
        $strint = substr(str_shuffle(str_repeat($int, 5)), 0, $lengthint);

        return $strstring.$strint;
    }

    public function dealerImport(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'file' => 'required',
      ]);
      if ($validator->passes()) {
        // $import = (new DealerImport())->queue($request->file('file'));

        $import = Excel::import(new DealerImport(), $request->file('file'));
        if ($import) {
          $response['redirect_url'] = route('superadmin.dealer.index');
          return $this->response(200, $response);
        }
      }else{
        $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
        return $this->response(400, $response);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dealer = User::find($id);
        $data['dealer'] =$dealer;
        $totalAnpao = 0;
        foreach ($dealer->angpao as $key => $value) {
          $totalAnpao = $totalAnpao + $value->bucket->nominal;
        }

        $data['totalangpao'] = $totalAnpao;
        return $this->response(200, $data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
          'name' => 'required',
          'dealer_code' => 'required',
        ]);

        if ($validator->passes()) {
          $dealer = User::find($id);
          $dealer->name = $request->name;
          $dealer->dealer_code = $request->dealer_code;
          $dealer->city = $request->city;
          $dealer->provance = $request->provance;

          if ($dealer->save()) {
            $response['redirect_url'] = route('superadmin.dealer.index');
            return $this->response(200, $response);
          }
        }else{
          $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
          return $this->response(400, $response);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
