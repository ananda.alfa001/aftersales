<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Bucket;
use App\Exports\CustomerExport;
use App\User;
use Carbon\Carbon;
use App\Entities\Customer;
use App\Entities\Configuration;
use Validator;
use App\Traits\Responder;

class DashboardController extends Controller
{
    use Responder;
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['buckets'] = Bucket::all();
        $totalAngpao = 0;
        $totalAngpaoTerbuka = 0;
        $totalAngpaoTerpakai = 0;
        $jumlahAngpao = 0;
        $jumlahAngpaoTerpakai = 0;


        $jumlahhadiah = 0;
        $totalhadiahterbuka=0;
        // foreach ($data['buckets'] as $key => $value) {
        //   $totalAngpao = $totalAngpao + ($value->nominal * $value->qty);
        //   $totalAngpaoTerbuka = $totalAngpaoTerbuka + ($value->angpao->count() * $value->nominal);
        //   $totalAngpaoTerpakai = $totalAngpaoTerpakai + ($value->angpao->count() * $value->nominal);
        //   $jumlahAngpao = $jumlahAngpao + $value->qty;
        //   $jumlahAngpaoTerpakai = $jumlahAngpaoTerpakai + $value->angpao->count();
        // }

        foreach ($data['buckets'] as $key => $value) {
          $totalAngpao = $totalAngpao + ($value->qty);
          $totalAngpaoTerbuka = $totalAngpaoTerbuka + ($value->angpao->count());
          $totalAngpaoTerpakai = $totalAngpaoTerpakai + ($value->angpao->count());
          $jumlahAngpao = $jumlahAngpao + $value->qty;
          $jumlahAngpaoTerpakai = $jumlahAngpaoTerpakai + $value->angpao->count();
        }

        $data['totalangpao'] = $totalAngpao;
        $data['totalangpaoterbuka'] = $totalAngpaoTerbuka;
        $data['totalangpaotersisa'] = $totalAngpao - $totalAngpaoTerpakai;
        $data['jumlahangpaotersisa'] = $jumlahAngpao - $jumlahAngpaoTerpakai;
        $data['dealers'] = User::role('Dealer')->get();
        return view('superadmin.dashboard.index',$data);
    }

    public function export($dealer,$start,$end)
    {
      if ($dealer == "all") {
        $namaDelaer = "Semua Dealer";
      }else{
        $namaDelaer = "Dealer ".User::find($dealer)->name;
      }

      $carbonStart = Carbon::createFromFormat("m-d-Y",$start)->format('Y-m-d 00:00:00');
      $carbonEnd = Carbon::createFromFormat("m-d-Y",$end)->format('Y-m-d 00:00:00');

      return (new CustomerExport($dealer,$carbonStart,$carbonEnd))->download("Data Excel {$namaDelaer} Tanggal {$carbonStart} -  {$carbonEnd} Mitsubishi.xlsx");
    }

    
    public function setting()
    {
      $data['config'] = Configuration::findOrFail(1);
      return view('superadmin.setting.index',$data);
    }

    public function settingWebsite(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'maintenance' => 'required',
      ]);

      if ($validator->passes()) {
        $config = Configuration::findOrFail(1);
        $config->maintenance = $request->maintenance;
        if ($config->save()) {
          $response['redirect_url'] = route('superadmin.setting');
          return $this->response(200, $response);
        }
      }else{
        $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
        return $this->response(400, $response);
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
