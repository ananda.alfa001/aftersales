<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Traits\Responder;
use App\Entities\Bucket;
use App\Entities\Customer;
use App\Entities\Angpao;


class WebsiteController extends Controller
{
  use Responder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->session()->has('idcustomer')){
          return redirect()->route('angpao.forminput');
        }else{
          return view('website.verify-page');
        }

    }

    public function formInput(Request $request)
    {
        if($request->session()->has('idcustomer')){
          $idcustomer = $request->session()->get('idcustomer');
          $data['customer'] = Customer::find($idcustomer);
          if ($data['customer']->name != null || $data['customer']->no_tlp != null ) {
            return redirect()->route('angpao.angpaotree');
          }

          return view('website.form-input',$data);
        }else{
          return redirect('/');
        }

    }

    public function angpaoTree(Request $request)
    {
      if($request->session()->has('idcustomer')){
        $idcustomer = $request->session()->get('idcustomer');
        $customer = Customer::find($idcustomer);
        if ($customer->angpao != null) {
          return redirect()->route('angpao.voucher');
        }

        return view('website.angpao-tree');
      }else{
        return redirect('/');
      }
    }

    public function voucher(Request $request)
    {
        if($request->session()->has('idcustomer')){
          $idcustomer = $request->session()->get('idcustomer');
          $data['customer'] = Customer::find($idcustomer);
          if ($data['customer']->angpao == null) {
            return redirect()->route('angpao.angpaotree');
          }

          return view('website.voucher',$data);
        }else{
          return redirect('/');
        }
    }

    public function doVerifyNoPol(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'no_pol' => 'required',
        ]);

        if ($validator->passes()) {
          $customer = Customer::where('no_pol',$request->no_pol)->first();
          if ($customer == null) {
            $response['message'] = array_merge(['data tidak ditemukan']);
            return $this->response(400, $response);
          }
          if ($customer->angpao != null) {
            $response['message'] = array_merge(['data sudah mempunyai angpao dan tidak bisa login kembali']);
            return $this->response(400, $response);
          }

          $request->session()->put('idcustomer',$customer->id);

          $response['redirect_url'] = route('angpao.forminput');
          return $this->response(200, $response);
        }else{
          $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
          return $this->response(400, $response);
        }
      }

      public function doInputForm(Request $request)
      {
          $validator = Validator::make($request->all(), [
            'name' => 'required',
            'no_tlp' => 'required',
          ]);

          if ($validator->passes()) {
            $idcustomer = $request->session()->get('idcustomer');
            $customer = Customer::where('id',$idcustomer)->first();
            $customer->name = strtoupper($request->name);
            $customer->no_tlp = $request->no_tlp;
            if ($customer->save()) {
              $response['redirect_url'] = route('angpao.angpaotree');
              return $this->response(200, $response);
            }
          }else{
            $response['message'] = array_merge(['Data Invalid!'], $validator->errors()->all());
            return $this->response(400, $response);
          }
        }

        public function doLottery(Request $request)
        {
          $buckets = Bucket::all();
          $datahabis = 0;
          $data = null;

          $idcustomer = $request->session()->get('idcustomer');
          $customer = Customer::find($idcustomer);

          if ($customer->angpao != null) {
            $response['message'] = array_merge(['Anda sudah mengambil kado!']);
            return $this->response(400, $response);
          }

          foreach ($buckets as $key => $bucket) {
            $countAngpao = $bucket->angpao->count();
            if ($countAngpao >= $bucket->qty) { //hbis
              $datahabis = $datahabis + $bucket->percentage;
            }else{
              $data[] = [
                "id" => $bucket->id,
                "nominal" => $bucket->nominal,
                "percentage" => $bucket->percentage,
              ];
            }
          }

          if ($datahabis != 0 && $data != null) {
            $data[0]['percentage'] += $datahabis;
          }

          $random = mt_rand(0, 100);
          $w = 0;
          $itemBucketId = null;
          foreach($data as $id => $value) { // chooses a number
              $w += $value['percentage'];
              if ($w > $random) {
                  $itemBucketId = $value['id'];
                  break;
              }
          }

          $angpao = new Angpao();
          $angpao->id_bucket = $itemBucketId;
          $angpao->id_customer = $idcustomer;
          $angpao->id_user = $customer->dealer->id;

          $code = $this->generate_unique_code(10);
          $check = Angpao::where('code', $code)->count();

          while ($check != 0) {
              $code = generate_unique_code(10);
              $check = User::where('password_real', $code)->count();
          }

          $angpao->code = $code;
          if ($angpao->save()) {
            $response['redirect_url'] = route('angpao.voucher');
            return $this->response(200, $response);
          }


        }

        function generate_unique_code($length =null)
        {
            $strstring = "ABCDEFGHIJKLMNPQRSTUV123456789";

            $int = "123456789";
            $code = substr(str_shuffle(str_repeat($strstring, 5)), 0, $length);

            return $code;
        }

        public function logout(Request $request)
        {
            $idcustomer = $request->session()->forget('idcustomer');
            return redirect()->route('home');
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $buckets = Bucket::all();
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
