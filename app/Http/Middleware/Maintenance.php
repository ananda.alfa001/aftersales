<?php

namespace App\Http\Middleware;

use Closure;
use App\Entities\Configuration;

class Maintenance
{
    public function handle($request, Closure $next)
    {
        $data['config'] = Configuration::findOrFail(1);
        if ($data['config']->maintenance == 1 && !$request->is(['login','logout','auth','superadmin','superadmin/*'])){
            return response()->view('errors.webdown', $data, 503);
        } else {
            return $next($request);
        }
    }
}
