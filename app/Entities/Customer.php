<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;



class Customer extends Model
{
    protected $table = "customers";
    protected $fillable = [
        'id_user','no_pol', 'work_order'
    ];

    public function angpao()
    {
        return $this->belongsTo('App\Entities\Angpao', 'id', 'id_customer');
    }

    public function dealer()
    {
        return $this->belongsTo('App\User', 'id_user');
    }
}
