<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Angpao extends Model
{
    public function bucket()
    {
        return $this->belongsTo('App\Entities\Bucket', 'id_bucket','id');
    }
}
