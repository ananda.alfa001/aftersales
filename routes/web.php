<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Website\WebsiteController@index')->name('home');
Route::get('/login', 'Auth\AuthAdminController@index')->name('admin-mitsubishi');
Route::post('/auth', 'Auth\AuthAdminController@dologin')->name('dologin');
Route::get('/logout', 'Auth\AuthAdminController@logout')->name('logout');


Route::group(['as' => 'angpao.', 'prefix' => '/angpao'], function () {
  Route::get('/form-input', 'Website\WebsiteController@formInput')->name('forminput');
  Route::post('/doverifynopol', 'Website\WebsiteController@doVerifyNoPol')->name('doverifynopol');
  Route::get('/anpaotree', 'Website\WebsiteController@angpaoTree')->name('angpaotree');
  Route::post('/doinputform', 'Website\WebsiteController@doInputForm')->name('doinputform');
  Route::post('/dolottery', 'Website\WebsiteController@doLottery')->name('dolottery');
  Route::get('/voucher', 'Website\WebsiteController@voucher')->name('voucher');
  Route::get('/logout', 'Website\WebsiteController@logout')->name('logout');

});

Route::group(['as' => 'superadmin.', 'prefix' => '/superadmin','middleware' => ['role:Super Admin']], function () {
    Route::get('/setting', 'SuperAdmin\DashboardController@setting')->name('setting');
    Route::post('/settingwebsite', 'SuperAdmin\DashboardController@settingWebsite')->name('settingwebsite');
    Route::get('/', 'SuperAdmin\DashboardController@index')->name('index');

    Route::group(['as' => 'dealer.', 'prefix' => '/dealer'], function () {
        Route::get('/', 'SuperAdmin\DealerController@index')->name('index');
        Route::post('/show/{id}', 'SuperAdmin\DealerController@show')->name('show');
        Route::post('/created', 'SuperAdmin\DealerController@store')->name('store');
        Route::post('/{id}/update', 'SuperAdmin\DealerController@update')->name('update');
        Route::post('/imports', 'SuperAdmin\DealerController@dealerImport')->name('dealerimport');
    });

    Route::group(['as' => 'angpao.', 'prefix' => '/angpao'], function () {
      Route::get('/', 'SuperAdmin\AngpaoController@index')->name('index');
      Route::post('/add-qty/{id}', 'SuperAdmin\AngpaoController@addQty')->name('addqty');
    });

    Route::get('/{dealer}/{start}/{end}/export', 'SuperAdmin\DashboardController@export')->name('customer.export');
});

Route::group(['as' => 'dealer.', 'prefix' => '/dealer','middleware' => ['role:Dealer']], function () {
    Route::get('/', 'Dealer\DealerController@index')->name('index');
    Route::get('/add-customer', 'Dealer\DealerController@AddCustomerIndex')->name('addcustomer');
    Route::get('/{id}/print', 'Dealer\DealerController@print')->name('print');
    Route::get('/search-customer', 'Dealer\DealerController@SearchCustomerIndex')->name('searchcustomer');
    Route::get('/list-customer', 'Dealer\DealerController@ListCustomerIndex')->name('listcustomer');
    Route::post('/{id}/show', 'Dealer\DealerController@show')->name('show');
    Route::post('/createcustomer', 'Dealer\DealerController@createCustomer')->name('createcustomer');
    Route::post('/{id}/update', 'Dealer\DealerController@update')->name('update');
    Route::get('/{dealer}/{start}/{end}/export', 'SuperAdmin\DashboardController@export')->name('customer.export');
    Route::post('/import_customer', 'Dealer\DealerController@import')->name('importcustomer');

});
