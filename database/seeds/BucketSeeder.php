<?php

use Illuminate\Database\Seeder;
use App\Entities\Bucket;

class BucketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bucket::create([
          'nominal' => 50000,
          'qty' => 13440,
          'percentage' => 84,
        ]);

        Bucket::create([
          'nominal' => 100000,
          'qty' => 800,
          'percentage' => 10,
        ]);

        Bucket::create([
          'nominal' => 150000,
          'qty' => 133,
          'percentage' => 2.5,
        ]);

        Bucket::create([
          'nominal' => 200000,
          'qty' => 60,
          'percentage' => 1.5,
        ]);

        Bucket::create([
          'nominal' => 500000,
          'qty' => 16,
          'percentage' => 1,
        ]);

        Bucket::create([
          'nominal' => 1000000,
          'qty' => 4,
          'percentage' => 0.5,
        ]);
    }
}
