<?php

use Illuminate\Database\Seeder;
use App\Entities\Configuration;


class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Configuration::create([
          'maintenance' => 0,
        ]);
    }
}
