<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAngpaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('angpaos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_bucket');
            $table->unsignedBigInteger('id_customer');
            $table->unsignedBigInteger('id_user');
            $table->string('code');
            $table->boolean('is_using')->default(1);
            $table->timestamps();

            $table
            ->foreign('id_bucket')
            ->references('id')
            ->on('buckets')
            ->onDelete('restrict');

            $table
            ->foreign('id_customer')
            ->references('id')
            ->on('customers')
            ->onDelete('restrict');

            $table
            ->foreign('id_user')
            ->references('id')
            ->on('users')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('angpaos');
    }
}
