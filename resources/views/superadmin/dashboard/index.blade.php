@extends('superadmin.template')
@section('content')
<div class="content-wrapper">

  <!-- Page header -->
  <div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
        <h4> <span class="font-weight-semibold">Home</span></h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
    </div>
  </div>
  <!-- /page header -->


  <!-- Content area -->
  <div class="content">

    <!-- Main charts -->
    <div class="row">
      <div class="col-xl-12">

        <!-- Traffic sources -->
        <div class="card">
        	<div class="card-header">
            <div class="row">
              <div class="col-md-3">
                <div class="input-group">
  								<span class="input-group-prepend">
  									<span class="input-group-text"><i class="icon-calendar22"></i></span>
  								</span>
  								<input type="text" class="form-control daterange-weeknumbers" name="date" value="02/01/2021 - 02/28/2021" onchange="reloadUrl()" readonly>
  							</div>
              </div>
              <div class="col-md-4">
                <select data-placeholder="Select your state" name="dealer" class="form-control form-control-select2" onchange="reloadUrl()">
									<option value="all">Semua After Sales</option>
                    <optgroup label="After Sales">
                    <?php foreach ($dealers as $key => $value): ?>
                      <option value="{{$value->id}}">{{$value->name}}</option>
                    <?php endforeach; ?>
                    </optgroup>
								</select>
              </div>
              <div class="col-md-5">
                <div class="" style="text-align:end;">
                  <a class="export-customer" href="#">
                    <button type="button" class="btn btn-light" >Export</button>
                  </a>
                </div>
              </div>
            </div>

        	</div>

          <!-- Quick stats boxes -->
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-4">

                <!-- Current server load -->
                <a href="">
                  <div class="card bg-primary">
                    <div class="card-body">
                      <div class="d-flex">
                        Total Kado
                        </div>
                      <div>
                        <h3 class="font-weight-semibold mb-0"><b>{{$totalangpao}}</b></h3>
                      </div>
                    </div>
                  </div>
                </a>
                <!-- /current server load -->

              </div>

              <div class="col-lg-4">

                <!-- Current server load -->
                <a href="">
                  <div class="card bg-warning">
                    <div class="card-body">
                      <div class="d-flex">
                        Kado Terbuka
                              </div>

                              <div>
                        <h3 class="font-weight-semibold mb-0"><b>{{number_format($totalangpaoterbuka)}}</b></h3>

                      </div>
                    </div>

                  </div>
                </a>
                <!-- /current server load -->

              </div>

              <div class="col-lg-4">

                <!-- Current server load -->
                <a href="">
                  <div class="card bg-success">
                    <div class="card-body">
                      <div class="d-flex">
                        Kado Tersisa
                              </div>

                              <div>
                                <h3 class="font-weight-semibold mb-0"><b>{{number_format($totalangpaotersisa)}}</b></h3>
                      </div>
                    </div>

                  </div>
                </a>
                <!-- /current server load -->

              </div>
            </div>
          </div>
          <!-- /quick stats boxes -->
          <div class="container-fluid">
            <div class="card">
    					<div class="card-header header-elements-inline">
    						<h5 class="card-title"><b>Daftar Kado</b></h5>
                <div class="header-elements">
                  <a href="{{route('superadmin.angpao.index')}}"><button type="button" class="btn btn-light" >Lihat Semua <i class="icon-arrow-right7 ml-2"></i></button></a>
              	</div>
    					</div>

    					<div class="table-responsive">
    						<table class="table">
    							<thead>
    								<tr>
    									<th><b>Kado Tersedia</b></th>
    									<th><b>Jumlah Kado</b></th>
    									<th><b>Kado Terbuka</b></th>
    									<th><b>Kado Tersisa</b></th>
    								</tr>
    							</thead>
    							<tbody>
                    <?php foreach ($buckets as $key => $bucket): ?>
      								<tr>
      									<td>{{$bucket->nominal}}</td>
      									<td>{{$bucket->qty}}</td>
      									<td>{{$bucket->angpao->count()}}</td>
      									<td>{{$bucket->qty - $bucket->angpao->count()}}</td>
      								</tr>
                    <?php endforeach; ?>
    							</tbody>
    						</table>
    					</div>
    				</div>
          </div>
        </div>
        <!-- /traffic sources -->

      </div>
    </div>
    <!-- /main charts -->


    <!-- Dashboard content -->

    <!-- /dashboard content -->

  </div>
  <!-- /content area -->


</div>
@endsection
@section('scripts')
<script src="{{asset('superuser_asset/global_assets/js/demo_pages/picker_date.js')}}"></script>
<script src="{{asset('superuser_asset/global_assets/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{asset('superuser_asset/global_assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
<script src="{{asset('superuser_asset/global_assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
<script src="{{asset('superuser_asset/global_assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
<script src="{{asset('superuser_asset/global_assets/js/demo_pages/form_layouts.js')}}"></script>

<script src="{{asset('superuser_asset/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('superuser_asset/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {

  var dealer = $('[name="dealer"]').val();
  var date = $('[name="date"]').val();
  var explode = date.split(" - ");

  var start = explode[0].split('/').join('-');
  var end = explode[1].split('/').join('-');

  var urlExportCustomer = '{{ route("superadmin.customer.export", [":dealer",":start",":end"]) }}';
  urlExportCustomer = urlExportCustomer.replace(':dealer', dealer);
  urlExportCustomer = urlExportCustomer.replace(':start', start);
  urlExportCustomer = urlExportCustomer.replace(':end', end);

  $('.export-customer').attr('href',urlExportCustomer);
});

function reloadUrl() {
  var dealer = $('[name="dealer"]').val();
  var date = $('[name="date"]').val();
  var explode = date.split(" - ");

  var start = explode[0].split('/').join('-');
  var end = explode[1].split('/').join('-');

  var urlExportCustomer = '{{ route("superadmin.customer.export", [":dealer",":start",":end"]) }}';
  urlExportCustomer = urlExportCustomer.replace(':dealer', dealer);
  urlExportCustomer = urlExportCustomer.replace(':start', start);
  urlExportCustomer = urlExportCustomer.replace(':end', end);

  $('.export-customer').attr('href',urlExportCustomer);
}
</script>

@endsection
