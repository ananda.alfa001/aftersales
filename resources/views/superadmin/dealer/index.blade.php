@extends('superadmin.template')
@section('styles')
<style media="screen">
  .form-group{
    margin-bottom: 0px;
  }
  .fz-16{
    font-size: 16px;
  }
  .fstyle-title{
    color: #757575;
    font-size: 12px;
  }
</style>

<link href="{{ asset('superuser_asset/plugins/fileuploads/css/dropify.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('content')
<div class="content-wrapper">

  <!-- Page header -->
  <div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
        <h4> <span class="font-weight-semibold">Dealer</span></h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
    </div>
  </div>
  <!-- /page header -->


  <!-- Content area -->
  <div class="content">

    <!-- Basic datatable -->
  				<div class="card">
  					<div class="card-header header-elements-inline">
  						<h5 class="card-title"><b>Daftar After Sales</b></h5>
              <div class="header-elements">
                <a href="#"><button type="button" class="btn btn-light mr-2 open-import" >Import</button></a>
                <button type="button" class="btn btn-danger modal-create" data-type="create"><i class="icon-plus3 mr-2"></i> Tambah</button>
              </div>
  					</div>

  					<table class="table table_dealer">
  						<thead>
  							<tr>
  								<th width="20%">Nama After Sales</th>
                  <th width="20%">E-mail</th>
  								<th width="20%">Jumlah Customer</th>
  								<th width="20%">Kado Terbuka</th>
  								<th >Actions</th>
  							</tr>
  						</thead>
  						<tbody>
                <?php foreach ($dealers as $key => $dealer): ?>
                  <tr>
                    <td>{{$dealer->name}}</td>
                    <td>{{$dealer->dealer_code}}</td>
                    <td>{{$dealer->customer->count()}}</td>
                    <td>{{$dealer->angpao->count()}}</td>
                    <td>
                      <button type="button" class="btn btn-danger mr-2 modal-detail" data-action="{{route('superadmin.dealer.show',$dealer->id)}}"  data-customer="{{$dealer->customer->count()}}" data-terbuka="{{$dealer->angpao->count()}}" data-terpakai="{{$dealer->angpao->where('is_using',1)->count()}}">Detail</button>
                      <a href="#"><button type="button" class="btn btn-light mr-2 modal-create" data-type="edit" data-id="{{$dealer->id}}" data-url="{{route('superadmin.dealer.show',$dealer->id)}}">Edit</button></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
  						</tbody>
  					</table>
  				</div>
  				<!-- /basic datatable -->
          <div id="create_dealer" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title-account">Tambah After Sales</h5>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form class="kt-form" id="superuser-form" data-action="" enctype="multipart/form-data" autocomplete="off">
                  @csrf
                  <div class="modal-body">
                    <div class="form-group row">
                      <label class="col-form-label col-sm-3">Nama After Sales</label>
                      <div class="col-sm-12">
                        <input type="text" class="form-control" placeholder="Nama After Sales" value="" name="name">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-form-label col-sm-3">E-mail</label>
                      <div class="col-sm-12">
                        <input type="text" class="form-control" placeholder="Email After Sales" value="" name="dealer_code">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-form-label col-sm-3">Kota</label>
                      <div class="col-sm-12">
                        <input type="text" class="form-control" placeholder="City" value="" name="city">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-form-label col-sm-3">Provinsi</label>
                      <div class="col-sm-12">
                        <input type="text" class="form-control" placeholder="Provance" value="" name="provance">
                      </div>
                    </div>

                  </div>

                  <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal"><b>Kembali</b></button>
                    <button type="submit" name="submit" class="btn bg-danger"><b>Tambah</b></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <!-- Detail -->
          <div id="detail_dealer" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title-account"></h5>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                  <div class="modal-body">
                    <div class="form-group row">
                      <label class="col-form-label col-sm-12 fstyle-title">Nama After Sales</label>
                      <div class="col-sm-12">
                        <p class="fz-16"><b class="text-nama">{{$dealer->name}}</b></p>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-form-label col-sm-12 fstyle-title">E-mail After Sales</label>
                      <div class="col-sm-12">
                        <p class="fz-16"><b class="text-dealercode">{{$dealer->dealer_code}}</b></p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-form-label col-sm-12 fstyle-title">Jumlah Customer</label>
                      <div class="col-sm-12">
                        <p class="fz-16 text-customer"><b>{{$dealer->customer->count()}}</b></p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-form-label col-sm-12 fstyle-title">Jumlah Kado Terbuka</label>
                      <div class="col-sm-12">
                        <p class="fz-16 text-terbuka"><b>{{$dealer->angpao->count()}}</b></p>
                      </div>
                    </div>
                    </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal"><b>Kembali</b></button>
                  </div>
              </div>
            </div>
          </div>

          <div id="import_dealer" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
              <form class="kt-form" id="form-import" data-action="{{route('superadmin.dealer.dealerimport')}}" enctype="multipart/form-data" autocomplete="off">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title-account"><b>Import Excel After Sales</b></h5>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    @csrf
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="col-md-12">
                            <input type="file" name="file" class="dropify" required data-height="300" data-default-file="" />
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="submit" name="submit" class="btn bg-danger"><b>Import</b></button>
                  <button type="button" class="btn btn-light" data-dismiss="modal"><b>Kembali</b></button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
  <!-- /content area -->
</div>
@section('scripts')
<script src="{{ asset('superuser_asset/plugins/fileuploads/js/dropify.min.js') }}"></script>
<script src="{{asset('superuser_asset/global_assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('superuser_asset/global_assets/js/demo_pages/datatables_basic.js')}}"></script>
<script src="{{asset('superuser_asset/form/form.js')}}"></script>
<script src="{{asset('superuser_asset/form/formimport.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {

  $('.table_dealer').dataTable({
    "responsive": true,
    "processing": true,
    "filter":true,
    "columnDefs": [{
        "orderable": false,
        "width": 100,
        "targets": [ 5 ]
    }],

  });

  $('.dropify').dropify({
        messages: {
            'default': 'Uploud file excel disini',
            'replace': 'Uploud file excel disini',
            'remove': 'Remove',
            'error': 'Ooops, something wrong appended.'
        },
        error: {
            'fileSize': 'The file size is too big (1M max).'
        }
    });
});

$(document).on('click','.modal-create',function(){
  var type = $(this).data('type');

    if (type == "edit") {
      var action = $(this).data('url');
      var id = $(this).data('id');

      $.ajaxSetup({
          headers:
          { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
      });

      $.post(action, function (data) {
          $('[name="name"]').val(data.data.dealer.name);
          $('[name="dealer_code"]').val(data.data.dealer.dealer_code);
          $('[name="city"]').val(data.data.dealer.city);
          $('[name="provance"]').val(data.data.dealer.provance);
      });
      var urlAEdit = '{{ route("superadmin.dealer.update", ":id") }}';
      urlAEdit = urlAEdit.replace(':id', id);
      $('#superuser-form').attr('data-action',urlAEdit);
      $('.modal-title-account').text('Edit After Sales');

      $('#create_dealer').modal('show');
    }else{
      $('#superuser-form').attr('data-action',"{{route('superadmin.dealer.store')}}");
      $('[name="name"]').val("");
      $('[name="dealer_code"]').val("");
      $('[name="city"]').val("");
      $('[name="provance"]').val("");
      $('.modal-title-account').text('Buat After Sales');

      $('#create_dealer').modal('show');
    }


 });

$(document).on('click','.open-import',function(){
   $('#import_dealer').modal('show');
});

$(document).on('click','.modal-detail',function(){
   var action = $(this).data('action');
   var terbuka = $(this).data('terbuka');
   var terpakai = $(this).data('terpakai');
   var customer = $(this).data('customer');
    $.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    $.post(action, function (data) {
        $('.text-nama').text(data.data.dealer.name);
        $('.text-dealercode').text(data.data.dealer.dealer_code);
        $('.text-customer b').text(customer);
        $('.text-terbuka b').text(terpakai);
        $('.text-terpakai b').text(terpakai);
        $('.text-angpao-terpakai b').text("Rp " +data.data.totalangpao.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    });
    $('#detail_dealer').modal('show');
});

</script>
@endsection
@endsection
