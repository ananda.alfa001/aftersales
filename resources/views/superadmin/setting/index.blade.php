@extends('superadmin.template')
@section('content')
<div class="content-wrapper">

  <!-- Page header -->
  <div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
        <h4> <span class="font-weight-semibold">Home</span></h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
    </div>
  </div>
  <!-- /page header -->


  <!-- Content area -->
  <div class="content">

    <!-- Main charts -->
    <div class="row">
      <div class="col-xl-12">

        <!-- Traffic sources -->
        <div class="card">
					<div class="card-header header-elements-inline">
						<h6 class="card-title">Setting Website</h6>
					</div>
          <form class="kt-form" id="superuser-form" data-action="{{route('superadmin.settingwebsite')}}" enctype="multipart/form-data" autocomplete="off">
            @csrf
  					<div class="card-body">
              <div class="form-group row">
  							<label class="col-form-label col-lg-3">Maintenance <span class="text-danger">*</span></label>
  							<div class="col-lg-9">
  								<select name="maintenance" class="form-control">
                    <option value="0" {{$config->maintenance == 0 ? 'selected' : ''}}>Disable</option>
  									<option value="1" {{$config->maintenance == 1 ? 'selected' : ''}}>Enable</option>
  								</select>
  							</div>
  						</div>

              <div class="d-flex justify-content-end align-items-center">
  							<button type="submit" class="btn btn-primary ml-3">Submit <i class="icon-paperplane ml-2"></i></button>
  						</div>
  					</div>
          </form>

				</div>
        <!-- /traffic sources -->

      </div>
    </div>
    <!-- /main charts -->

  </div>
  <!-- /content area -->


</div>
@endsection
@section('scripts')
<script src="{{asset('superuser_asset/form/form.js')}}"></script>
@endsection
