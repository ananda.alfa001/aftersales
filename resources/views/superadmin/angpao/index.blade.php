@extends('superadmin.template')
@section('styles')
<style media="screen">
  .form-group{
    margin-bottom: 0px;
  }
  .fz-16{
    font-size: 16px;
  }
  .fstyle-title{
    color: #757575;
    font-size: 12px;
  }
  .disabled-input{
    background-color: #E8E8E8 !important;
    color: #2A2A2A !important;
  }
</style>
@endsection
@section('content')
<div class="content-wrapper">

  <!-- Page header -->
  <div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
        <h4> <span class="font-weight-semibold">Kado</span></h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
    </div>
  </div>
  <!-- /page header -->


  <!-- Content area -->
  <div class="content">

    <!-- Basic datatable -->
  				<div class="card">
  					<div class="card-header header-elements-inline">
  						<h5 class="card-title"><b>Daftar Kado</b></h5>
  					</div>

  					<table class="table table_dealer">
  						<thead>
  							<tr>
  								<th width="20%">Kado Tersedia</th>
  								<th width="20%">Jumlah Kado</th>
  								<th width="20%">Kado Terbuka</th>
  								<th width="20%">Kado Tersisa</th>
  								<th >Actions</th>
  							</tr>
  						</thead>
  						<tbody>
                <?php foreach ($buckets as $key => $bucket): ?>
                  <tr>
                    <td>{{$bucket->nominal}}</td>
                    <td>{{$bucket->qty}}</td>
                    <td>{{$bucket->angpao->count()}}</td>
                    <td>{{$bucket->qty - $bucket->angpao->count()}}</td>
                    <td>
                      <button type="button" class="btn btn-danger mr-2 modal-add" data-nominal="{{$bucket->nominal}}" data-id="{{$bucket->id}}">Tambah</button>
                      <a href="#"><button type="button" class="btn btn-light mr-2 modal-detail" data-nominal="{{$bucket->nominal}}" data-angpao="{{$bucket->qty}}" data-terbuka="{{$bucket->angpao->count()}}" data-tersisa="{{$bucket->qty - $bucket->angpao->count()}}" >Detail</button></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
  						</tbody>
  					</table>
  				</div>
  				<!-- /basic datatable -->
          <div id="add_angpao" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title-account"><b>Tambah Kado</b></h5>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form class="kt-form" id="superuser-form" data-action="" enctype="multipart/form-data" autocomplete="off">
                  @csrf
                  <div class="modal-body">
                    <div class="form-group row">
                      <label class="col-form-label col-sm-3">Nama Kado</label>
                      <div class="col-sm-12">
                        <input type="text" class="form-control disabled-input" placeholder="Tulis Nominal Kado" value="{{$bucket->nominal}}" name="nominal" readonly disabled>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-form-label col-sm-3">Tambahan Kado</label>
                      <div class="col-sm-12">
                        <input type="text" class="form-control" placeholder="Tulis tambahan kado" value="" name="qty">
                      </div>
                    </div>

                  </div>

                  <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal"><b>Kembali</b></button>
                    <button type="submit" name="submit" class="btn bg-danger"><b>Tambah</b></button>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div id="detail_dealer" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title-account"><b>Detail Kado</b></h5>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                  <div class="modal-body">
                    <div class="form-group row">
                      <label class="col-form-label col-sm-12 fstyle-title">Nama Kado</label>
                      <div class="col-sm-12">
                        <p class="fz-16"><b class="text-nominal">0</b></p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-form-label col-sm-12 fstyle-title">Jumlah Kado</label>
                      <div class="col-sm-12">
                        <p class="fz-16"><b class="text-angpao">0</b></p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-form-label col-sm-12 fstyle-title">Kado Terbuka</label>
                      <div class="col-sm-12">
                        <p class="fz-16"><b class="text-terbuka">0</b></p>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-form-label col-sm-12 fstyle-title">Kado Tersisa</label>
                      <div class="col-sm-12">
                        <p class="fz-16"><b class="text-tersisa">0</b></p>
                      </div>
                    </div>

                  </div>

                  <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal"><b>Kembali</b></button>
                  </div>
              </div>
            </div>
          </div>
          
        </div>
  <!-- /content area -->
</div>
@endsection
@section('scripts')
<script src="{{asset('superuser_asset/global_assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('superuser_asset/global_assets/js/demo_pages/datatables_basic.js')}}"></script>
<script src="{{asset('superuser_asset/form/form.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('.table_dealer').dataTable({
    "responsive": true,
    "processing": true,
    "filter":true,
    "order": [[1, 'desc']],
    "columnDefs": [{
        "orderable": false,
        "width": 100,
        "targets": [ 4 ]
    }],

  });
});

$(document).on('click','.modal-add',function(){
    var nominal = $(this).data('nominal');
    var id = $(this).data('id');
    $('[name="nominal"]').val(nominal);
    $('#add_angpao').modal('show');
    var urlAddQty = '{{ route("superadmin.angpao.addqty", ":id") }}';
    urlAddQty = urlAddQty.replace(':id', id);

    $('#superuser-form').attr('data-action', urlAddQty);

 });

 $(document).on('click','.modal-detail',function(){
     var nominal = $(this).data('nominal');
     var angpao = $(this).data('angpao');
     var terbuka = $(this).data('terbuka');
     var tersisa = $(this).data('tersisa');

     $('.text-nominal').text(nominal);
     $('.text-angpao').text(angpao);
     $('.text-terbuka').text(terbuka);
     $('.text-tersisa').text(tersisa);
     $('#detail_dealer').modal('show');
  });

</script>
@endsection
