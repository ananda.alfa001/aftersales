<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{asset('superuser_asset/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">

    <title>Mitsubishi Bagi-Bagi Angpao</title>
    <style media="screen">


    </style>
  </head>
  <body>
    <div class="container-fluid">
      <div class="col-md-12">
        <div class="body-content" style="margin-top: 10%;text-align: center;">
          <div class="row">
            <div class="col-md-12">
              <img src="{{asset('superuser_asset/assets/img/logo-mitshubishi-red.svg')}}" style="width: 30%;" class="img-logo" alt="">
            </div>
            <div class="col-md-12">
                <img src="{{asset('superuser_asset/assets/img/logo-event-red.png')}}" class="img-event" style="margin-top: 5%;width: 50%;" alt="">
            </div>
            <div class="col-md-12 center box-information" style="text-align: center;margin-bottom: 32px;">
              <p class="nominal" style="font-size: 100px;color: #D53231;font-weight: 800;margin: 30px 0;">Rp {{number_format($customer->angpao->bucket->nominal)}},-</p>
              <div class="form-information" style=" margin-bottom: 20px;">
                <label style="color: black;font-size: 26px;margin-bottom: 0;">Nama</label>
                <p><b style="color: black;font-size: 32px">{{$customer->name}}</b></p>
              </div>
              <div class="form-information" style=" margin-bottom: 20px;">
                <label style="color: black;font-size: 26px;margin-bottom: 0;">Nomor Polisi</label>
                <p><b style="color: black;font-size: 32px">{{$customer->no_pol}}</b></p>
              </div>
              <div class="form-information" style=" margin-bottom: 20px;">
                <label style="color: black;font-size: 26px;margin-bottom: 0;">Kode Angpao</label>
                <p><b style="color: black;font-size: 32px">{{$customer->angpao->code}}</b></p>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
