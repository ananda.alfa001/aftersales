<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Mitsubishi Bagi-Bagi Angpao</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{asset('superuser_asset/assets/img/favicon.png')}}">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('superuser_asset/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('superuser_asset/assets/plugins/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style media="screen">
      body{
        background-color: #D53231;
      }
      .body-content{
        margin-top: 15%;
        text-align: center;
      }
      .img-logo{
        width: 30%;
      }
      .img-event{
        margin-top: 16px;
        width: 85%;
      }
      .button-verify{
        background-color: white;
        font-weight: 600;
        margin-top: 2%;
        color: #D53231;
      }
      .form-input{
        background-color: #AE2423;
        border: 1px solid #AE2423;
        color: #FFFFFF;
      }
      .form-input::placeholder{
        color: #C2C2C2;
      }
      .form-insert{
        margin: 10% 10%;
      }
      .btn-primary:hover{
        background-color: white;
        color: #D53231;
      }
      .form-box{
        margin-bottom: 8px;
      }
      .form-input.disable-form{
        background-color: #C32928;
      }
    </style>
  </head>
  <body>
  <div class="container">
    <div class="col-md-12">
      <div class="body-content">
        <div class="row">
          <div class="col-md-12">
            <img src="{{asset('superuser_asset/assets/img/logo2-mitsubishi.svg')}}" class="img-logo" alt="">
          </div>
          <div class="col-md-12">
            <img src="{{asset('superuser_asset/assets/img/logo-event-crop.png')}}" class="img-event" alt="">
          </div>
        </div>
        <form class="kt-form" id="superuser-form" data-action="{{route('dealer.createcustomer')}}" enctype="multipart/form-data" autocomplete="off">
          @csrf
          <div class="form-insert">
            <div class="form-box">
              <input type="text" class="form-control form-input" name="no_pol" value="" placeholder="Masukkan NIK">
            </div>
            <div class="form-box">
              <input type="text" class="form-control form-input" name="work_order" value="" placeholder="Tulis nama department">
            </div>
            <div class="row" style="margin: 0 auto;display: inline-flex;">
              <div class="col-xs-6">
                <button type="button" class="btn button-verify mr-2" data-toggle="modal" data-target="#importExcel">Import File</button>
              </div>
              <div class="col-xs-6">
                <button type="submit" class="btn button-verify mr-2">Daftar</button>
              </div>
              <div class="col-xs-6">
                <a href="{{route('dealer.index')}}">
                  <button type="button" class="btn button-verify ">Kembali</button>
                </a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="POST" class="kt-form" id="import-form" data-action="{{route('dealer.importcustomer')}}" enctype="multipart/form-data">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
						</div>
						<div class="modal-body">
 
							{{ csrf_field() }}
 
							<label>Pilih File Excel</label>
							<div class="form-group">
								<input type="file" name="file" required="required">
							</div>
 
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Import</button>
						</div>
					</div>
				</form>
			</div>
		</div>

  <script src="{{asset('superuser_asset/global_assets/js/main/jquery.min.js')}}"></script>
  <script src="{{asset('superuser_asset/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('superuser_asset/assets/plugins/toastr/toastr.min.js') }}"></script>
  <script src="{{asset('superuser_asset/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>

  <script type="text/javascript">

		String.prototype.UcFirst = function() {
			return this.charAt(0).toUpperCase() + this.substr(1);
		}

		function redirect(url, timer) {
			if (url.includes('datatable')) {
				$(url).DataTable().ajax.reload();
				return;
			}

			setTimeout(function() {
				if (url == 'reload()') {
					window.location.reload();
				} else {
					window.location.href = url;
				}
			}, timer)
		}

		function toastMessage(type, object) {
			var time = 0;

			if (type == 'undefined' || type == null) {
				return false;
			}

			if ($.isEmptyObject(object)) {
				toastr[type](type.UcFirst());
			} else {
				$.each(object, function(index, value) {
					setTimeout(function() {
						toastr[type](value);
					}, time);
					time += 500;
				});
			}

			return time;
		}
	</script>
  <script src="{{asset('superuser_asset/form/form.js')}}"></script>
  </body>
</html>
