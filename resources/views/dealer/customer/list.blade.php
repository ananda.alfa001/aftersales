@extends('dealer.template')
@section('styles')
<style media="screen">
  .form-group{
    margin-bottom: 0px;
  }
  .fz-16{
    font-size: 16px;
  }
  .fstyle-title{
    color: #757575;
    font-size: 12px;
  }
  .header-search{
    padding: 30px 30px;
    border-bottom: 1px solid #ddd;
  }
  .datatable-header{
    display: none;
  }
</style>
<link rel="stylesheet" type="text/css" href="{{asset('superuser_asset/assets/plugins/print/print.min.css')}}">

@endsection
@section('content')
<div class="content-wrapper">

  <!-- Page header -->
  <div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
        <h4> <span class="font-weight-semibold">Daftar Customer</span></h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
    </div>
  </div>
  <!-- /page header -->


  <!-- Content area -->
  <div class="content">


    <!-- Basic datatable -->
    <div class="card">
      <div class="card-header header-elements-inline">
        <div class="col-md-3" style="padding-left: 0px">
          <div class="input-group">
            <span class="input-group-prepend">
              <span class="input-group-text"><i class="icon-calendar22"></i></span>
            </span>
            <input type="text" class="form-control daterange-weeknumbers" name="date" value="02/01/2021 - 02/28/2021" onchange="reloadUrl()" readonly>
          </div>
        </div>
        <div class="header-elements">
          <a class="export-customer" href="#">
            <button type="button" class="btn btn-light" >Export</button>
          </a>
        </div>
      </div>

        <div class="card-body">
          <ul class="nav nav-tabs">
            <li class="nav-item"><a href="#basic-tab1" class="nav-link active" data-toggle="tab">Dengan Kado</a></li>
            <li class="nav-item"><a href="#basic-tab2" class="nav-link" data-toggle="tab">Tanpa Kado</a></li>
          </ul>

          <div class="tab-content">
            <div class="tab-pane fade show active" id="basic-tab1">
              <table class="table table_dealer">
                <thead>
                  <tr>
                    <th width="15%">Nama Customer</th>
                    <th width="15%">NIK</th>
                    <th width="15%">Department</th>
                    <th width="15%">Kado</th>
                    <th width="15%">Kode Kado</th>
                    <th >Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($custhasangpao as $key => $value): ?>
                    <tr>
                      <td>{{$value->name}}</td>
                      <td>{{$value->no_pol}}</td>
                      <td>{{$value->work_order}}</td>
                      <td>{{$value->angpao->bucket->nominal}}</td>
                      <td>{{$value->angpao->code}}</td>
                      <td>
                        <button type="button" class="btn btn-danger mr-2 modal-detail" onclick="printExternal('{{route('dealer.print',$value->id)}}')">Print</button>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>

            <div class="tab-pane fade show" id="basic-tab2">
              <table class="table table_dealer">
                <thead>
                  <tr>
                    <th width="15%">Nama Customer</th>
                    <th width="15%">NIK</th>
                    <th width="15%">Department</th>
                    <th width="15%">Kado</th>
                    <th width="15%">Kode Kado</th>
                    <th >Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($custhasntangpao as $key => $value): ?>
                    <tr>
                      <td>{{$value->name}}</td>
                      <td>{{$value->no_pol}}</td>
                      <td>{{$value->work_order}}</td>
                      <td>-</td>
                      <td>-</td>
                      <td>
                        <button type="button" class="btn btn-light mr-2 modal-input-customer" data-customer="{{route('dealer.show',$value->id)}}" data-id="{{$value->id}}">Edit</button>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>

      <div id="input_customer" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title-account">Edit Customer</h5>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form class="kt-form" id="superuser-form" data-action="" enctype="multipart/form-data" autocomplete="off">
              @csrf
              <div class="modal-body">
                <div class="form-group row">
                  <label class="col-form-label col-sm-3">Nama Customer</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" placeholder="Nama Customer" value="" name="name">
                    <input type="hidden" name="type" value="list">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-form-label col-sm-3">Nomer Polisi*</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" placeholder="Nomer Polisi" value="" name="no_pol">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-form-label col-sm-3">Nomer Work Order*</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" placeholder="Nomer Work Order" value="" name="work_order">
                  </div>
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal"><b>Kembali</b></button>
                <button type="submit" name="submit" class="btn bg-danger"><b>Edit</b></button>
              </div>
            </form>
          </div>
        </div>
      </div>

        </div>
  <!-- /content area -->
</div>
@endsection
@section('scripts')
<script src="{{asset('superuser_asset/global_assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('superuser_asset/global_assets/js/demo_pages/datatables_basic.js')}}"></script>
<script src="{{asset('superuser_asset/form/form.js')}}"></script>
 <script src="{{asset('superuser_asset/assets/plugins/print/print.min.js')}}"></script>
 <script src="{{asset('superuser_asset/global_assets/js/demo_pages/picker_date.js')}}"></script>
 <script src="{{asset('superuser_asset/global_assets/js/plugins/pickers/daterangepicker.js')}}"></script>
 <script src="{{asset('superuser_asset/global_assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
 <script src="{{asset('superuser_asset/global_assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
 <script src="{{asset('superuser_asset/global_assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
 <script src="{{asset('superuser_asset/global_assets/js/demo_pages/form_layouts.js')}}"></script>
 <script type="text/javascript">
 $(document).ready(function() {

   var dealer = $('[name="dealer"]').val();
   var date = $('[name="date"]').val();
   var explode = date.split(" - ");

   var start = explode[0].split('/').join('-');
   var end = explode[1].split('/').join('-');

   var urlExportCustomer = '{{ route("dealer.customer.export", [":dealer",":start",":end"]) }}';
   urlExportCustomer = urlExportCustomer.replace(':dealer', "{{Auth::user()->id}}");
   urlExportCustomer = urlExportCustomer.replace(':start', start);
   urlExportCustomer = urlExportCustomer.replace(':end', end);

   $('.export-customer').attr('href',urlExportCustomer);
 });

 function reloadUrl() {
   var dealer = $('[name="dealer"]').val();
   var date = $('[name="date"]').val();
   var explode = date.split(" - ");

   var start = explode[0].split('/').join('-');
   var end = explode[1].split('/').join('-');

   var urlExportCustomer = '{{ route("dealer.customer.export", [":dealer",":start",":end"]) }}';
   urlExportCustomer = urlExportCustomer.replace(':dealer', "{{Auth::user()->id}}");
   urlExportCustomer = urlExportCustomer.replace(':start', start);
   urlExportCustomer = urlExportCustomer.replace(':end', end);

   $('.export-customer').attr('href',urlExportCustomer);
 }
 </script>
<script type="text/javascript">
function printExternal(url) {
  $.get(url, function (data) {
    printJS({
      printable:data,
      type:'raw-html',
      font_size : '240pt',
    });
  });
}
$(document).ready(function() {

  $('.table_dealer').dataTable({
    "responsive": true,
    "processing": true,
    "filter":true,
     "bLengthChange": false,
    "searching":false,
    "columnDefs": [{
        "orderable": false,
        "width": 100,
        "targets": [ 5 ]
    }],

  });
});

$(document).on('click','.modal-input-customer',function(){
   var customer = $(this).data('customer');
   var id = $(this).data('id');
    $.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    $.post(customer, function (data) {
      $('[name="name"]').val(data.data.customer.name);
      $('[name="no_pol"]').val(data.data.customer.no_pol);
      $('[name="work_order"]').val(data.data.customer.work_order);
    });
    var urlAEdit = '{{ route("dealer.update", ":id") }}';
    urlAEdit = urlAEdit.replace(':id', id);
    $('#superuser-form').attr('data-action',urlAEdit);

    $('#input_customer').modal('show');
});

</script>
@endsection
