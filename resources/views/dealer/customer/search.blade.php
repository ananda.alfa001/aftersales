@extends('dealer.template')
@section('styles')
<style media="screen">
  .form-group{
    margin-bottom: 0px;
  }
  .fz-16{
    font-size: 16px;
  }
  .fstyle-title{
    color: #757575;
    font-size: 12px;
  }
  .header-search{
    padding: 30px 30px;
    border-bottom: 1px solid #ddd;
  }
  .datatable-header{
    display: none;
  }
  .display-none{
    display: none;
  }

</style>
<link rel="stylesheet" type="text/css" href="{{asset('superuser_asset/assets/plugins/print/print.min.css')}}">

@endsection
@section('content')
<div class="content-wrapper">

  <!-- Page header -->
  <div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
        <h4> <span class="font-weight-semibold">Cari Customer</span></h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
    </div>
  </div>
  <!-- /page header -->


  <!-- Content area -->
  <div class="content">


    <!-- Basic datatable -->
  				<div class="card">
            <form method="GET" class="">
              <div class="container-fluid header-search">
                <div class="row text-center">
                  <div class="col col1">
                    <div class="form-group form-group-feedback form-group-feedback-left">
    									<input type="text" class="form-control form-control-lg" name="customer" value="{{ request()->get('customer') }}" placeholder="Cari nama customer">
    									<div class="form-control-feedback form-control-feedback-lg">
    										<i class="icon-search4" style="color:#C2C2C2"></i>
    									</div>
    								</div>
                  </div>
                  <div class="col col2">
                    <div class="form-group form-group-feedback form-group-feedback-left">
    									<input type="text" class="form-control form-control-lg" name="no_polisi" value="{{ request()->get('no_polisi') }}" placeholder="Cari NIK">
    									<div class="form-control-feedback form-control-feedback-lg">
    										<i class="icon-search4" style="color:#C2C2C2"></i>
    									</div>
    								</div>
                  </div>
                  <div class="col col3">
                    <div class="form-group form-group-feedback form-group-feedback-left">
    									<input type="text" class="form-control form-control-lg" name="no_work" value="{{ request()->get('no_work') }}" placeholder="Cari Nama Department">
    									<div class="form-control-feedback form-control-feedback-lg">
    										<i class="icon-search4" style="color:#C2C2C2"></i>
    									</div>
    								</div>
                  </div>
                  <div class="col col4">
                    <div class="form-group form-group-feedback form-group-feedback-left">
    									<input type="text" class="form-control form-control-lg" name="code_angpao" value="{{ request()->get('code_angpao') }}" placeholder="Cari kode kado">
    									<div class="form-control-feedback form-control-feedback-lg">
    										<i class="icon-search4" style="color:#C2C2C2"></i>
    									</div>
    								</div>
                  </div>
                  <div class="col col5 text-left">
                    <button type="submit" class="btn btn-danger mr-2 refresh-table" >Cari</button>
                  </div>
              </div>
            </form>
            </div>
            <?php if (request()->get("customer") != null || request()->get("no_polisi") != null || request()->get("no_work") != null || request()->get("code_angpao") != null): ?>

  					<table class="table table_dealer">
  						<thead>
  							<tr>
  								<th width="15%">Nama Customer</th>
  								<th width="15%">NIK</th>
  								<th width="15%">Department</th>
                  <th width="15%">Kado</th>
  								<th width="15%">Kode Kado</th>
  								<th >Actions</th>
  							</tr>
  						</thead>
  						<tbody>
                <?php foreach ($customer as $key => $value): ?>
                  <tr>
    								<td>{{$value->name}}</td>
    								<td>{{$value->no_pol}}</td>
    								<td>{{$value->work_order}}</td>
                    <td>{{$value->angpao == null ?  '-': $value->angpao->bucket->nominal}}</td>
                    <td>{{$value->angpao == null ? '-' : $value->angpao->code}}</td>
    								<td>
                        <button type="button" class="btn btn-light mr-2 modal-input-customer" data-customer="{{route('dealer.show',$value->id)}}" data-id="{{$value->id}}">Edit</button>
                    </td>
    							</tr>
                <?php endforeach; ?>
  						</tbody>
  					</table>
            <?php endif; ?>
  				</div>
          <?php foreach ($customer as $key => $value): ?>
          <div id="input_customer" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title-account">Edit Customer</h5>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form class="kt-form" id="superuser-form" data-action="" enctype="multipart/form-data" autocomplete="off">
                  @csrf
                  <div class="modal-body">
                    <div class="form-group row">
                      <label class="col-form-label col-sm-3">Nama Customer</label>
                      <div class="col-sm-12">
                        <input type="text" class="form-control" placeholder="Nama Customer" value="" name="name">
                        <input type="hidden" name="type" value="search">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-form-label col-sm-3">NIK</label>
                      <div class="col-sm-12">
                        <input type="text" class="form-control" placeholder="Nomer Polisi" value="" name="no_pol">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-form-label col-sm-3">Department</label>
                      <div class="col-sm-12">
                        <input type="text" class="form-control" placeholder="Nomer Work Order" value="" name="work_order">
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal"><b>Kembali</b></button>
                    <button type="submit" name="submit" class="btn bg-danger"><b>Edit</b></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <?php endforeach; ?>
        </div>
  <!-- /content area -->
</div>
@endsection
@section('scripts')
<script src="{{asset('superuser_asset/global_assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('superuser_asset/global_assets/js/demo_pages/datatables_basic.js')}}"></script>
<script src="{{asset('superuser_asset/assets/plugins/print/print.min.js')}}"></script>

<script type="text/javascript">

function printExternal(url) {
  $.get(url, function (data) {
    printJS({
      printable:data,
      type:'raw-html',
      font_size : '240pt',
    });
  });
}

$(document).on('click','.modal-input-customer',function(){
   var customer = $(this).data('customer');
   var id = $(this).data('id');
    $.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    $.post(customer, function (data) {
      $('[name="name"]').val(data.data.customer.name);
      $('[name="no_pol"]').val(data.data.customer.no_pol);
      $('[name="work_order"]').val(data.data.customer.work_order);
    });
    var urlAEdit = '{{ route("dealer.update",":id")}}';
    urlAEdit = urlAEdit.replace(':id', id);
    $('#superuser-form').attr('data-action',urlAEdit);
    $('#input_customer').modal('show');
});

$(document).ready(function() {
  if ('{{request()->get("customer") != null || request()->get("no_polisi") != null || request()->get("no_work") != null || request()->get("code_angpao") != null }}') {
    $('.table_dealer').dataTable({
      "responsive": true,
      "processing": true,
      "filter":true,
      "searching":false,
      "columnDefs": [{
          "orderable": false,
          "width": 100,
          "targets": [ 5 ]
      }],
    });
  }
});



</script>
@endsection
