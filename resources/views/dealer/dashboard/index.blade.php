@extends('dealer.template')
@section('styles')
<style media="screen">
@media screen and (max-width: 992px){
  .img-event{
    margin-bottom: 16px;
    width: 100%;
  }
}
</style>
@endsection
@section('content')
<div class="content-wrapper">

  <!-- Page header -->
  <div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
        <h4> <span class="font-weight-semibold">Dashboard</span></h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
    </div>
  </div>
  <!-- /page header -->


  <!-- Content area -->
  <div class="content">

    <!-- Main charts -->
    <div class="row">
      <div class="col-md-12">

        <!-- Traffic sources -->
        <img src="{{asset('superuser_asset/assets/img/logo-mitshubishi-red-big.png')}}" class="img-event" alt="">

        <!-- /traffic sources -->

      </div>
    </div>
    <!-- /main charts -->


    <!-- Dashboard content -->

    <!-- /dashboard content -->

  </div>
  <!-- /content area -->


</div>
@endsection
@section('scripts')
@endsection
