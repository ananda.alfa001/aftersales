<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Mitsubishi Bagi-Bagi Angpao</title>

  <!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('superuser_asset/global_assets/css/icons/icomoon/styles.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('superuser_asset/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('superuser_asset/assets/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('superuser_asset/assets/css/layout.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('superuser_asset/assets/css/components.min.css')}}" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" href="{{asset('superuser_asset/assets/img/favicon.png')}}">
	<link href="{{asset('superuser_asset/assets/css/colors.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('superuser_asset/assets/plugins/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{asset('superuser_asset/assets/css/mystyle.css')}}" rel="stylesheet" type="text/css">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<style media="screen">
		.navbar-dark{
			background-color: #1A1A1A;
		}
		.sidebar-dark{
			background-color: #0D0D0D;
		}
	</style>
	<!-- /global stylesheets -->
  @yield('styles')

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark">
		<div class="navbar-brand">
			<a href="{{route('dealer.index')}}" class="d-inline-block">
				<img src="{{asset('superuser_asset/assets/img/logomitsubishi.png')}}" style="height:auto" alt="">
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>

			</ul>

			<span class="badge bg-success ml-md-3 mr-md-auto">Online</span>

			<ul class="navbar-nav">
				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
						<img src="{{asset('superuser_asset/global_assets/images/placeholders/placeholder.jpg')}}" class="rounded-circle mr-2" height="34" alt="">
						<span>{{Auth::user()->name}}</span>
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						<a href="{{route('logout')}}" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">

		<!-- Main sidebar -->
		<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

			<!-- Sidebar mobile toggler -->
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				Navigation
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<!-- /sidebar mobile toggler -->


			<!-- Sidebar content -->
			<div class="sidebar-content">

				<!-- User menu -->
				<div class="sidebar-user">
					<div class="card-body">
						<div class="media">
							<div class="mr-3">
								<a href="#"><img src="{{asset('superuser_asset/global_assets/images/placeholders/placeholder.jpg')}}" width="38" height="38" class="rounded-circle" alt=""></a>
							</div>

							<div class="media-body">
								<div class="media-title font-weight-semibold">{{Auth::user()->name}}</div>
								<div class="font-size-xs opacity-50">
									{{Auth::user()->getRoleNames()->first()}}
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /user menu -->


				<!-- Main navigation -->
				<div class="card card-sidebar-mobile">
					<ul class="nav nav-sidebar" data-nav-type="accordion">

						<!-- Main -->
						<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
						<li class="nav-item">
							<a href="{{route('dealer.index')}}" class="nav-link {{ request()->routeIs('dealer.index') ? 'active' : '' }}">
								<i class="icon-home4"></i>
								<span>
									Home
								</span>
							</a>
						</li>
            <li class="nav-item">
							<a href="{{route('dealer.addcustomer')}}" class="nav-link {{ request()->routeIs('dealer.addcustomer') ? 'active' : '' }}">
								<i class="icon-user-plus"></i>
								<span>
									Tambah Customer
								</span>
							</a>
						</li>
            <li class="nav-item">
							<a href="{{route('dealer.searchcustomer')}}" class="nav-link {{ request()->routeIs('dealer.searchcustomer') ? 'active' : '' }}">
								<i class="icon-search4"></i>
								<span>
									Cari Customer
								</span>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{route('dealer.listcustomer')}}" class="nav-link {{ request()->routeIs('dealer.listcustomer') ? 'active' : '' }}">
								<i class="icon-user"></i>
								<span>
									Daftar Customer
								</span>
							</a>
						</li>
						<!-- /page kits -->

					</ul>
				</div>
				<!-- /main navigation -->

			</div>
			<!-- /sidebar content -->

		</div>
		<!-- /main sidebar -->


		<!-- Main content -->
    @yield('content')
		<!-- /main content -->

	</div>
	<!-- /page content -->
	<!-- Core JS files -->
	<script src="{{asset('superuser_asset/global_assets/js/main/jquery.min.js')}}"></script>
	<script src="{{asset('superuser_asset/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
	<script src="{{asset('superuser_asset/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="{{asset('superuser_asset/global_assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
	<script src="{{asset('superuser_asset/global_assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
	<script src="{{asset('superuser_asset/global_assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
	<script src="{{asset('superuser_asset/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script src="{{asset('superuser_asset/global_assets/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script src="{{asset('superuser_asset/global_assets/js/plugins/pickers/daterangepicker.js')}}"></script>
	<script src="{{asset('superuser_asset/assets/plugins/toastr/toastr.min.js') }}"></script>

	<script src="{{asset('superuser_asset/assets/js/app.js')}}"></script>
	<script src="{{asset('superuser_asset/global_assets/js/demo_pages/dashboard.js')}}"></script>
	<script src="{{asset('superuser_asset/assets/plugins/toastr/toastr.min.js') }}"></script>
	<script type="text/javascript">

		String.prototype.UcFirst = function() {
			return this.charAt(0).toUpperCase() + this.substr(1);
		}

		function redirect(url, timer) {
			if (url.includes('datatable')) {
				$(url).DataTable().ajax.reload();
				return;
			}

			setTimeout(function() {
				if (url == 'reload()') {
					window.location.reload();
				} else {
					window.location.href = url;
				}
			}, timer)
		}

		function toastMessage(type, object) {
			var time = 0;

			if (type == 'undefined' || type == null) {
				return false;
			}

			if ($.isEmptyObject(object)) {
				toastr[type](type.UcFirst());
			} else {
				$.each(object, function(index, value) {
					setTimeout(function() {
						toastr[type](value);
					}, time);
					time += 500;
				});
			}

			return time;
		}
	</script>
	@yield('scripts')

</body>
</html>
