<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Mitsubishi Bagi-Bagi Angpao</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{asset('superuser_asset/assets/img/favicon.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('superuser_asset/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('superuser_asset/assets/css/animate.min.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style media="screen">
      body{
        background-color: #FFFFFF;
      }
      .body-content{
        margin-top: 15%;
        text-align: center;
      }
      .img-logo{
        width: 30%;
      }
      .img-event{
        margin-top: 5%;
        width: 90%;
      }
      .title-event{
        margin-bottom: 0 !important;
        color: #D53231;
        margin: 16px 0;
        font-weight: 800;
      }
      .desc-event{
        color: #D53231;
        font-weight: 600;
      }
      .modal-content{
        border: none;
        background: transparent;
      }
      .modal-content{
        margin-top: 50%;
        box-shadow: none;
      }
      .box-gift{
        text-align: center;
      }
      .display-none{
        display: none;
      }

    </style>
  </head>
  <body class="body-style display-none">
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="body-content">
        <div class="row">
          <div class="col-md-12">
            <img src="{{asset('superuser_asset/assets/img/logo-mitshubishi-red.svg')}}" class="img-logo" alt="">
          </div>
          <div class="col-md-12">
            <div class="text-box">
              <h1 class="title-event">PILIH KADOMU</h1>
            </div>
          </div>
          <div class="col-md-12">
              <img src="{{asset('superuser_asset/assets/img/angpaotree.png')}}" onclick="alert('mohon tunggu sampai data terunggah semua')" class="img-event" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
   <div class="modal animated bounceIn" id="myModal"  id="gif"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body box-gift">
                <img class="img-gif"src="{{asset('superuser_asset/assets/img/angpao-small.gif')}}" width="100%" alt="">
            </div>
        </div>
    </div>
  </div>

  <script src="{{asset('superuser_asset/global_assets/js/main/jquery.min.js')}}"></script>
  <script src="{{asset('superuser_asset/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('superuser_asset/assets/plugins/gif/freezeframe.min.js')}}"></script>

  <script type="text/javascript">
  window.addEventListener('load', (event) => {
    console.log('page is fully loaded');
    $('.body-style').removeClass('display-none');
    $('.img-event').attr('onclick','doLottery(Event)');
  });
  const gif = new Freezeframe('#gif', {
    trigger: false
  });

    function doLottery(e) {
      // e.preventDefault();
      gif.start();
    }

    gif.on('start', (items) => {
      $('#myModal').modal('show');

      var post_data = new FormData();
      $.ajaxSetup({
        headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });
      $.ajax({
          url: "{{route('angpao.dolottery')}}",
          data: post_data,
          contentType: false,
          cache: false,
          processData: false,
          dataType: 'json',
          type: 'POST',
          beforeSend: function() {

          },
          success: function(response) {
            setTimeout(function() {
                window.location.href = response.data.redirect_url;
            }, 2000);
          },
          error: function(request, status, error) {

          }
      });

    });
  </script>
  </body>
</html>
