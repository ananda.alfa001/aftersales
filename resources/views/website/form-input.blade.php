<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Mitsubishi Bagi-Bagi Kado</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('superuser_asset/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="{{asset('superuser_asset/assets/img/favicon.png')}}">
    <link href="{{ asset('superuser_asset/assets/plugins/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style media="screen">
      body{
        background-color: #D53231;
      }
      .body-content{
        margin-top: 15%;
        text-align: center;
      }
      .img-logo{
        width: 30%;
      }
      .img-event{
        margin-top: 16px;
        width: 50%;
      }
      .button-verify{
        width: 100%;
        background-color: white;
        font-weight: 600;
        margin-top: 2%;
        color: #D53231;
      }
      .form-input{
        background-color: #AE2423;
        border: 1px solid #AE2423;
        color: #FFFFFF;
      }
      .form-input::placeholder{
        color: #C2C2C2;
      }
      .form-insert{
        margin: 10% 10%;
      }
      .btn-primary:hover{
        background-color: white;
        color: #D53231;
      }
      .form-box{
        margin-bottom: 8px;
      }
      .form-input.disable-form{
        background-color: #C32928;
      }
    </style>
  </head>
  <body>
  <div class="container">
    <div class="col-md-12">
      <div class="body-content">
        <div class="row">
          <div class="col-md-12">
            <img src="{{asset('superuser_asset/assets/img/logo2-mitsubishi.svg')}}" class="img-logo" alt="">
          </div>
          <div class="col-md-12">
            <img src="{{asset('superuser_asset/assets/img/logo-event-crop.png')}}" class="img-event" alt="">
          </div>
        </div>
        <form class="kt-form" id="superuser-form" data-action="{{route('angpao.doinputform')}}" enctype="multipart/form-data" autocomplete="off">
          @csrf
          <div class="form-insert">
            <div class="form-box">
              <input type="text" class="form-control form-input disable-form" value="{{$customer->dealer->name}}" placeholder="Dealer Name" disabled>
            </div>
            <div class="form-box">
              <input type="text" class="form-control form-input disable-form" value="{{$customer->no_pol}}" placeholder="NIK" disabled>
            </div>
            <div class="form-box">
              <input type="text" class="form-control form-input disable-form" value="{{$customer->work_order}}" placeholder="Department" disabled>
            </div>
            <div class="form-box">
              <input type="text" class="form-control form-input" name="name" style="text-transform:uppercase" value="{{@$customer->name}}" placeholder="nama lengkap" required>
            </div>
            <div class="form-box">
              <input type="text" class="form-control form-input" name="no_tlp" value="{{@$customer->no_tlp}}" placeholder="nomor telepon">
            </div>
            <button type="submit" class="btn button-verify">Ambil Kado</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script src="{{asset('superuser_asset/global_assets/js/main/jquery.min.js')}}"></script>
  <script src="{{asset('superuser_asset/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('superuser_asset/assets/plugins/toastr/toastr.min.js') }}"></script>
  <script src="{{asset('superuser_asset/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>

  <script type="text/javascript">

		String.prototype.UcFirst = function() {
			return this.charAt(0).toUpperCase() + this.substr(1);
		}

		function redirect(url, timer) {
			if (url.includes('datatable')) {
				$(url).DataTable().ajax.reload();
				return;
			}

			setTimeout(function() {
				if (url == 'reload()') {
					window.location.reload();
				} else {
					window.location.href = url;
				}
			}, timer)
		}

		function toastMessage(type, object) {
			var time = 0;

			if (type == 'undefined' || type == null) {
				return false;
			}

			if ($.isEmptyObject(object)) {
				toastr[type](type.UcFirst());
			} else {
				$.each(object, function(index, value) {
					setTimeout(function() {
						toastr[type](value);
					}, time);
					time += 500;
				});
			}

			return time;
		}
	</script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('#superuser-form').submit(function(e) {
        e.preventDefault();

        $('#superuser-form').find("button[type='submit']").prop('disabled', true);

        var post_data = new FormData(this);


        if ($('#img').length) {
            var name = $('#img').data('name').toString();
            var img = $('#img')[0].files[0];
            post_data.append(name, defaultFor(img, ""));
        }

        $.ajax({
            url: $('#superuser-form').data('action'),
            data: post_data,
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            type: 'POST',
            beforeSend: function() {
              toastr["info"]("Mohon ditunggu");
              $.blockUI({
                  message: 'Mohon ditunggu',
                  overlayCSS: {
                      backgroundColor: '#1b2024',
                      opacity: 0.8,
                      cursor: 'wait'
                  },
                  css: {
                      border: 0,
                      color: '#fff',
                      padding: 0,
                      backgroundColor: 'transparent'
                  }
              });

              $('#superuser-form').find("button[type='submit']").text("Mohon tunggu");

            },
            success: function(response) {
                let toast = toastMessage('success', response.data.message);
                if (response.data.redirect_url) {
                    redirect(response.data.redirect_url, toast += 500);
                }
            },
            error: function(request, status, error) {
              $.unblockUI();
              $('#superuser-form').find("button[type='submit']").text("Ambil Angpao");
              $('#superuser-form').find("button[type='submit']").prop('disabled', false);
              toastMessage('error', request.responseJSON.data.message);
            }
          });
        });
      });

  </script>
  </body>
</html>
