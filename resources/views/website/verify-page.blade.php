<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Mitsubishi Bagi-Bagi Angpao</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{asset('superuser_asset/assets/img/favicon.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  	<link href="{{asset('superuser_asset/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
  	<link href="{{ asset('superuser_asset/assets/plugins/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style media="screen">
      body{
        background-color: #D53231;
      }
      .body-content{
        margin-top: 15%;
        text-align: center;
      }
      .img-logo{
        width: 35%;
      }
      .img-event{
        margin-top: 16px;
        width: 85%;
      }
      .button-verify{
        width: 100%;
        background-color: white;
        font-weight: 600;
        color: #D53231;
      }
      .form-no-polisi{
        margin-bottom: 10px;
        background-color: #AE2423;
        border: 1px solid #AE2423;
        color: #FFFFFF;
      }
      .form-no-polisi::placeholder{
        color: #C2C2C2;
      }
      .form-insert{
        margin: 10% 10%;
      }
      .btn-primary:hover{
        background-color: white;
        color: #D53231;
      }

    </style>
  </head>
  <body>
  <div class="container">
    <div class="col-md-12">
      <div class="body-content">
        <div class="row">
          <div class="col-md-12">
            <img src="{{asset('superuser_asset/assets/img/logo2-mitsubishi.svg')}}" class="img-logo" alt="">
          </div>
          <div class="col-md-12">
            <img src="{{asset('superuser_asset/assets/img/logo-event-crop.png')}}" class="img-event" alt="">
          </div>
        </div>
        <form class="kt-form" id="superuser-form" data-action="{{route('angpao.doverifynopol')}}" enctype="multipart/form-data" autocomplete="off">
          @csrf
          <div class="form-insert">
            <input type="text" class="form-control form-no-polisi" name="no_pol" placeholder="Masukkan NIK">
            <button type="submit" class="btn button-verify">Verifikasi</button>
          </div>
        </form>

      </div>
    </div>
  </div>
  <script src="{{asset('superuser_asset/global_assets/js/main/jquery.min.js')}}"></script>
  <script src="{{asset('superuser_asset/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('superuser_asset/assets/plugins/toastr/toastr.min.js') }}"></script>
  <script src="{{asset('superuser_asset/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>

  <script type="text/javascript">

		String.prototype.UcFirst = function() {
			return this.charAt(0).toUpperCase() + this.substr(1);
		}

		function redirect(url, timer) {
			if (url.includes('datatable')) {
				$(url).DataTable().ajax.reload();
				return;
			}

			setTimeout(function() {
				if (url == 'reload()') {
					window.location.reload();
				} else {
					window.location.href = url;
				}
			}, timer)
		}

		function toastMessage(type, object) {
			var time = 0;

			if (type == 'undefined' || type == null) {
				return false;
			}

			if ($.isEmptyObject(object)) {
				toastr[type](type.UcFirst());
			} else {
				$.each(object, function(index, value) {
					setTimeout(function() {
						toastr[type](value);
					}, time);
					time += 500;
				});
			}

			return time;
		}
	</script>
  <script src="{{asset('superuser_asset/form/form.js')}}"></script>


  </body>
</html>
