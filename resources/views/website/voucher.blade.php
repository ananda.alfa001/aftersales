<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Mitsubishi Bagi-Bagi Angpao</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="{{asset('superuser_asset/assets/img/favicon.png')}}">
    <link href="{{asset('superuser_asset/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <style media="screen">
      body{
        background-image:  url("{{asset('superuser_asset/assets/img/backround-angpao.png')}} ");
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover;
        background-color: #D53231;
      }
      .body-content{
        margin-top: 15%;
        text-align: center;
      }
      .img-logo{
        width: 30%;
      }
      .img-event{
        margin-top: 16px;
        width: 90%;
      }
      .title-event{
        margin-bottom: 0 !important;
        font-size: 30px;
        color: #FFFFFF;
        margin: 2% 0;
        font-weight: 800;
      }
      .desc-event{
        font-size: 14px;
        color: #FFFFFF;
        font-weight: 600;
      }
      .nominal{
        font-size: 40px;
        color: #F7D954;
        font-weight: 800;
        margin: 30px 0;
      }
      .text-box{
        margin-top: 16px;
      }
      .form-information label{
        color: white;
        font-size: 14px;
        margin-bottom: 0;
      }
      .form-information p{
        color: white;
        font-size: 20px;
        margin-bottom: 20px;
      }
      .footer{
        color: white;
        font-size: 300%;
        margin-top: 10%;
        margin-bottom: 10%;
      }
      .button-verify{
        width: 100%;
        background-color: white;
        font-weight: 600;
        color: #D53231;
      }
      .button-back-box{
        text-align: center;
        padding: 30px 15%;
      }
    </style>
  </head>
  <body>
  <div class="container backround-angpao">
    <div class="col-md-12">
      <div class="body-content">
        <div class="row">
          <div class="col-md-12">
            <img src="{{asset('superuser_asset/assets/img/logo2-mitsubishi.svg')}}" class="img-logo" alt="">
          </div>
          <div class="col-md-12">
            <div class="text-box">
              <p class="title-event">SELAMAT</p>
              <p class="desc-event">ANDA MENDAPATKAN KADO</p>
            </div>
            <p class="nominal">{{$customer->angpao->bucket->nominal}}</p>
            <div class="form-information">
              <label>Nama</label>
              <p><b>{{$customer->name}}</b></p>
            </div>
            <div class="form-information">
              <label>NIK</label>
              <p><b>{{$customer->no_pol}}</b></p>
            </div>
            <div class="form-information">
              <label>Kode Kado</label>
              <p><b>{{$customer->angpao->code}}</b></p>
            </div>
          </div>

          <div class="button-back-box col-md-12">
            <a href="{{route('angpao.logout')}}">
              <button type="submit" class="btn button-verify">Back</button>
            </a>
          </div>

        </div>
      </div>
    </div>
  </div>

  <script src="{{asset('superuser_asset/global_assets/js/main/jquery.min.js')}}"></script>
  <script src="{{asset('superuser_asset/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
  </body>
</html>
